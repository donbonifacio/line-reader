require 'rubygems'
require 'bundler'

Bundler.require

file_name = ENV['FILE_NAME']

puts "Indexing file '#{file_name}'..."
require './lib/line/reader/index/memory'
index = Line::Reader::Index::Memory.new(file_name)
index.call

require './lib/line/reader/server.rb'

Line::Reader::Server.set :index, index
Line::Reader::Server.set :file_name, file_name

run Line::Reader::Server
