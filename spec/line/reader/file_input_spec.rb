# frozen_string_literal: true

require 'spec_helper'
require 'line/reader/file_input'

RSpec.describe Line::Reader::FileInput do
  context 'when loading test_file.txt' do
    let(:file_input) { described_class.new('spec/resources/test_file.txt') }

    describe '#line_at' do
      it 'is able to load specific lines' do
        expect(file_input.line_at(0)).to eql('Line one')
        expect(file_input.line_at(9)).to eql('Line two')
        expect(file_input.line_at(18)).to eql('Line three')
      end

      it 'handles out of range input' do
        expect do
          file_input.line_at(1000)
        end.to raise_error(EOFError)
      end

      it 'handles invalid input' do
        expect do
          file_input.line_at(-1)
        end.to raise_error(Errno::EINVAL)
      end
    end

    describe '#stream_line_at' do
      it 'is able to stream specific lines' do
        content = StringIO.new
        file_input.stream_line_at(9) do |c|
          content.write(c)
        end
        expect(content.string).to eql('Line two')
      end

      it 'handles out of range input' do
        expect do
          file_input.stream_line_at(1000)
        end.to raise_error(EOFError)
      end

      it 'handles invalid input' do
        expect do
          file_input.stream_line_at(-1)
        end.to raise_error(Errno::EINVAL)
      end
    end
  end
end
