require 'spec_helper'
require 'line/reader/index/memory'

RSpec.describe Line::Reader::Index::Memory do
  context 'when indexing test_file.txt' do
    let(:indexer) { described_class.new('spec/resources/test_file.txt') }

    it 'is able to store all the line locations' do
      indexer.call
      expect(indexer.line_position(1)).to eql(0)
      expect(indexer.line_position(2)).to eql(9)
      expect(indexer.line_position(3)).to eql(18)
      expect(indexer.line_position(4)).to eql(29)
      expect(indexer.line_position(5)).to eql(30)
    end

    it 'returns nil on invalid lines' do
      indexer.call
      expect(indexer.line_position(-1)).to be_nil
    end
  end

  context 'when indexing file does not exist' do
    let(:indexer) { described_class.new('spec/resources/does_not_exist') }

    it 'throws proper exception' do
      expect do
        indexer.call
      end.to raise_error(Errno::ENOENT)
    end
  end
end
