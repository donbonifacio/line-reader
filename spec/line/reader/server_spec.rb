require 'spec_helper'
require 'rack/test'
require 'line/reader/server'
require 'line/reader/index/memory'

RSpec.describe Line::Reader::Server do
  include Rack::Test::Methods

  def app
    file_name = './spec/resources/test_file.txt'
    index = Line::Reader::Index::Memory.new(file_name)
    index.call

    described_class.set :index, index
    described_class.set :file_name, file_name
    described_class
  end

  it 'gets lines successfully' do
    get '/lines/3'
    expect(last_response).to be_ok
    expect(last_response.body.strip).to eq('Line three')
  end

  it 'return 413 if line not found' do
    get '/lines/123'
    expect(last_response.status).to be(413)
  end

  it 'return 400 if bad input' do
    get '/lines/random'
    expect(last_response.status).to be(400)
  end

  it 'return 404 for invalid routes' do
    get '/something_else'
    expect(last_response.status).to be(404)
  end
end
