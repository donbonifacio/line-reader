# Line::Reader

This implementation uses Ruby and Sinatra. `build.sh` will just `bundle install`
and `run.sh` runs the server.

Also included a `run-docker.sh` that will conveniently setup a container and run it.

## Quick start

If you have ruby:

```
./build.sh
./run.sh README.md
curl http://localhost:9292/lines/1
 => "# Line::Reader"
```

`bundle exec rspec` will run the test suite.

If you have docker:

```
./run-docker.sh README.md
curl http://localhost:9292/lines/1
 => "# Line::Reader"
```

# How does your system work?

I started by realizing that to fetch a particular line from a file, I'd need
to read the file until that line. I knew that I could jump in to a specific byte position
with a seek, and that could help me but only if I somehow knew where all the
lines start.

A straightforward approach would be to do a sequential search on the file. But
that would not scale for larger files. So, in an analogy with databases, I went
for an _index_ approach. The application will start by indexing the file,
storing in memory all the line positions. This is done in:

`lib/line/reader/index/memory.rb`

Having the position for each line, I'd need to fetch a line from that position.
I created a simple `FileInput` class that wraps this operation. It is able to
fetch a full line or to stream that line, char by char. This is done in:

`lib/line/reader/file_input.rb`

To bundle everything together I setup a simple sinatra application that streams
the requested lines to the client via. This is done in:

`lib/line/reader/server.rb`

The entry point for the app that sets everything up is:

`config.ru`

# How will your system perform with a 1 GB file? a 10 GB file? a 100 GB file?

With a large file the bottleneck will be the file indexing. It could be too slow
but we could improve that process with a divide to conquer approach. For example
mapping several parts of the file to several threads/processes and then reducing
them to a final index.

The index itself could be to big to fit in memory. I considered that and that's a
drawback of the current implementation. A way to _fix_ this would be to create
another index class that would store the index information in a file. For
example a binary stream of integers (line positions). I could then fetch the
position with a:

```
f.seek(line_number * POS_SIZE)
line_position = f.readint
```

I didn't do this, but if this class implemented the memory index's API it would
be a simple swap and the rest of the system should work properly™.

To be honest I don't know how the system would behave on 100 GB. I'd have to try
and see it running. In theory it seems to me that it would work. But with big
numbers some unknowns may show themselves (overflows? seek api not supporting
large numbers? does a ruby hash support that many entries?).

# How will your system perform with 100 users? 10000 users? 1000000 users?

I'll assume that the app would be running on an instance/machine with necessary
resources (connections, threads, ...). If what is the case then the problem with
many users could be memory usage. The exercise says that "a line can fit the
memory". But if we have several big lines maybe they won't fit the memory and we
get an out of memory exception.

With this in mind I modeled my approach to use streams from the file to the
server.

Again in theory this should work, but would have to try it for tons of
concurrent connections. Probably a super instance could support this. But if we
couldn't scale this vertically we'd need to review other options.

# What documentation, websites, papers, etc did you consult in doing this assignment?

The sinatra documentation, and google. :) 

I never used sinatra so I had to search a bit on how to perform some actions
(streaming data, testing, etc).

# What third-party libraries or other tools does the system use? How did you choose each library or framework you used?

I got sinatra because I wanted a simple and lightweight approach. I tried to
focus more on the "logic implementation" classes and keep the dependencies
minimal.

# How long did you spend on this exercise? If you had unlimited more time to spend on this, how would you spend it and how would you prioritize each item?

It took me 1-3 hours to implement it, but I already had some hammock time to
consider my options and how to tackle the exercise.

I believe the solution is very simple and honestly at this time I'd require more
context before continuing.

* Who are the users and what are their typical use cases?
* What kind of files are we serving?
* What kind of SLAs should we target?
* How many users? Typical size of files?
* Do we control the generation of the input file? Could we generate the index at
  that point?

This would allow me to better consider my options and course of action (for
example is the main improvement point memory management or boot speed?).

Regardless of this information, I'd do:

1. Add logging/instrumentation
1. Implement the file index
1. Implement a multithreaded file index loader
1. Implement a system/integration test that would serve a large file and create
   several _bots_ to _spam_ the server. Agree on target response times and boot
   time and have it on the build pipeline for performance regressions

I should also have tried the app with big files. I tried it with the file at
https://norvig.com/big.txt - it has `128 457` lines and 7.1M. Everything was
fast and smooth. But the size is very far away from the other target question.

EDIT: Ok I duplicated a bit that big file and created one with 272M and `5 395 194` lines
and the indexing was much slower (30s) while then the running app was fast as
expected.

# If you were to critique your code, what would you have to say about it?

Hum... not functional enough. Indeed this is very IO-specific but I like to
favour pure functions and tend to dislike typical loops. But this logic is very
loop friendly. I also don't know if Ruby has better stream support that I could
use somehow. I'd have to do a bit of research on that.

I should also have separated the logic from the delivery mechanism. But this was
too simple and small. Even so I should have a better separation.

Error handling is not good. I tested for some edge case scenarios but I should
have wrapped/handled all exceptions with better context. For example performing `GET
/lines/random` should yield a proper error message: "'random' is not a valid
integer". Of if we don't provide a file to the run script, give that message
directly instead of just an IO error. This could also be on the "what do do with
more time" question.

Doing `bundle gem line-reader` was not that cool. I should have done
`line_reader` instead and now I have big namespaces and I found that
unnecessary.

Reading and writing char by char maybe it's too conservative. Ideally we'd
fetch a bundle of data (configurable size) from the file and write that bundle
directly to the HTTP response stream.
