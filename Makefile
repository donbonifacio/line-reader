.PHONY: help

help: ## Shows the available commands.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

install: ## Installs dependencies.
	bundle install

run: ## Runs the server
	FILE_NAME=$(FILE_NAME) bundle exec rackup

run-docker: ## Runs the server in a docker container.
	$ docker run -it --rm --name line-reader \
		-v "$$PWD":/usr/src/myapp \
		-w /usr/src/myapp \
		-p "9292:9292" \
		ruby:2.6 \
		make install && make run FILE_NAME=$(FILE_NAME)

test: ## Runs the test suite.
	bundle exec rspec

lint: ## Runs the linter.
	bundle exec rubocop
