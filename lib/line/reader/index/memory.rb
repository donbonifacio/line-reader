# frozen_string_literal: true

module Line
  module Reader
    module Index
      # Given a file name as a parameter, the `Line::Reader::Index::Memory`
      # class will walk through the file and store an index with the byte
      # position of each line. We can then ask for the position of arbitrary
      # lines.
      #
      # @example
      #   index = Line::Reader::Index::Memory.new('file.txt')
      #   index.call # index the file
      #   index.line_position(342) # byte position of line 342
      #
      class Memory
        attr_accessor :file_name, :index

        def initialize(file_name)
          self.file_name = file_name
          self.index = {}
        end

        def line_position(line_number)
          index[line_number]
        end

        def call
          position = 0
          line_number = 1 # Or zero?

          File.open(file_name, 'r') do |f|
            while !f.eof? && f.readline
              index[line_number] = position
              position = f.pos
              line_number += 1
            end
          end

          index.freeze
        end
      end
    end
  end
end
