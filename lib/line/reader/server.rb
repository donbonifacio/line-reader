require 'sinatra'
require 'sinatra/streaming'
require 'byebug'
require_relative 'file_input'

module Line
  module Reader
    # The HTTP server that will serve `/lines/:number`
    class Server < Sinatra::Base
      set :show_exceptions, false
      helpers Sinatra::Streaming

      attr_accessor :index

      get '/lines/:index' do
        target_line = Integer(params[:index], 10)

        position = settings.index.line_position(target_line)
        return status(413) if position.nil?

        file_input = Line::Reader::FileInput.new(settings.file_name)

        stream do |out|
          file_input.stream_line_at(position) do |c|
            out.putc c
          end
          out.flush
        end
      end

      error TypeError, ArgumentError do |e|
        logger.error e
        status 400
      end
    end
  end
end
