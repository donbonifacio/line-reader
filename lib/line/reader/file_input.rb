

module Line
  module Reader
    # `Line::Reader::FileInput` is able to open a file at a specific position
    # and return the line from that position. It's possible to obtain the
    # complete line directly or to stream the line byte by byte.
    #
    # @example
    #   file_input = Line::Reader::FileInput.new(file.txt)
    #   file_input.line_at(0) # Get full line from position 0
    #   file_input.stream_line_at(0) # Stream line byte by byte from position 0
    #
    class FileInput
      attr_accessor :file_name

      def initialize(file_name)
        self.file_name = file_name
      end

      def line_at(position)
        File.open(file_name, 'r') do |f|
          f.seek(position)
          f.readline.strip
        end
      end

      def stream_line_at(position)
        File.open(file_name, 'r') do |f|
          f.seek(position)
          raise EOFError if f.eof?

          while !f.eof? && (c = f.readchar) != "\n"
            yield c
          end
        end
      end
    end
  end
end
